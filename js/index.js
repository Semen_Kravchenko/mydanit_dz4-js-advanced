//Теория
//AJAX или Асинхронный JavaScript и XML - это метод, с помощью которого мы можем получать или передавать какие-нибудь данные на сервер без перезагрузки страницы в пределах одного веб-сервера или сайта, используя JS.

//Задание
const requestURL = 'https://swapi.dev/api/films/';

function sendRequest(url) {
    return fetch(url).then(response => response.json());
}

sendRequest(requestURL)
    .then(data => {
        const [...films] = data.results;

        const root = document.querySelector('.root');
        const ul = document.createElement('ul');
        root.append(ul);
        for (const key in films) {
            const {characters, episode_id, title, opening_crawl} = films[key];
            let line = document.createElement('hr');

            for (const key in characters) {
                sendRequest(characters[key]).then(data => {
                    let {name} = data;
                    characters[key] = name;
                });
            }

            const charList = document.createElement('ul');
            const charListTitle = document.createElement('h4');

            charList.style.border = '1px solid black';
            charList.style.paddingBottom = '10px';
            charListTitle.textContent = 'Characters';

            //animation
            const clock = document.createElement('div');
            clock.className = 'cssload-clock';

            charList.append(charListTitle, clock);

            setTimeout(() => {
                console.log(characters);
                for (const key in characters) {
                    let charItem = document.createElement('li');
                    charItem.style.color = 'red';
                    charItem.textContent = characters[key];
                    clock.remove();
                    charList.append(charItem);
                }
            }, 5000);


            let li = document.createElement('li');

            let id = document.createElement('span');
            id.textContent = 'EPISODE: ' + episode_id;

            let name = document.createElement('h3');
            name.textContent = title;

            let desc = document.createElement('p');
            desc.textContent = opening_crawl;

            li.append(id, name, charList, desc);
            ul.append(li, line);
        }
    })
    .catch(err => console.log(err));
